<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\logincred;
use App\Http\Controllers\applyreimb;
use App\Http\Controllers\satour;
use App\Http\Controllers\sapurchasematerial;
use App\Http\Controllers\salabour;
use App\Http\Controllers\saother;
use App\Http\Controllers\salaryadvance;
use App\Http\Controllers\loansalary;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('loginnew');
});

Route::get('/admin', function () {
    return view('admin.dashboard');
});



Route::get('/salaryadvance', function () {
    return view('admin.salaryadvance');
});

Route::get('/applyloan', function () {
    return view('admin.applyloan');
});





Route::view("loginnew","loginnew");

Route::post('adminpage', [logincred::class, 'validd']);
Route::get('logout', [logincred::class, 'logoutnow']);
Route::get('/applyreimb', [applyreimb::class, 'senddropdown1']);
Route::post('/applyreimbform', [applyreimb::class, 'insertdata']);
Route::get('/satour', [satour::class, 'senddropdown1']);
Route::post('/satourform', [satour::class, 'insertdata']);
Route::get('/sapurchasematerial', [sapurchasematerial::class, 'senddropdown1']);
Route::post('/sapurchasematerialform', [sapurchasematerial::class, 'insertdata']);
Route::get('/salabour', [salabour::class, 'senddropdown1']);
Route::post('/salabourform', [salabour::class, 'insertdata']);
Route::get('/saother', [saother::class, 'senddropdown1']);
Route::post('/saotherform', [saother::class, 'insertdata']);
Route::post('/salaryadvanceform', [salaryadvance::class, 'insertdata']);
Route::post('/applyloanform', [loansalary::class, 'insertdata']);
