@if(session('user')=='')
<script>window.location='loginnew'</script>
@endif
@extends('admin.master');



@section('content');
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0" style="color:green;">Apply for Loan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Apply for Loan</li>
           
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
   
    <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Loan Against Salary</title>


  </head>
  <body>
      <center>
      <h4>Apply Loan Against Salary</h4>
      <div class="shadow-lg p-3 mb-5 bg-white rounded" style="margin-left:1cm;margin-right:1cm;border-width:1px;border-color:black;border-style:solid;">
<form action="applyloanform" method="post">
    @csrf

<table class="table">
<thead>
</thead>
<tbody>
<tr><td>Require Amount</td>
    <td><input type="text" name="a1" class="form-control"></td>
</tr>



<tr><td>Purpose</td>

<td><textarea name="a2" rows="4"  class="form-control"></textarea></td>
</tr>

<tr>
    <td>Select Repayment Period</td>
<td><select name="a4" class="form-control">
<option value="1">1 Month</option>
<option value="2">2 Months</option>
<option value="3">3 Months</option>
<option value="4">4 Months</option>
<option value="5">5 Months</option>
<option value="6">6 Months</option>


</select></td></tr>
</tbody>
</table>
<input type="submit" name="a3" class="btn btn-primary">

</form>
</div>
</center>
 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
@endsection