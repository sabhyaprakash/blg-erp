@if(session('user')=='')
<script>window.location='loginnew'</script>
@endif
@extends('admin.master');



@section('content');
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0" style="color:green;">Site Advance</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Site Advance</li>
              <li class="breadcrumb-item active">Purchase Material</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
   
    <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Site Advance</title>


  </head>
  <body>
      <center>
      <h4>Site Advance : Purchase Material</h4>
      <div class="shadow-lg p-3 mb-5 bg-white rounded" style="margin-left:1cm;margin-right:1cm;border-width:1px;border-color:black;border-style:solid;">
<form action="sapurchasematerialform" method="post">
    @csrf

<table class="table">
<thead>
</thead>
<tbody>
<tr><td>Material Name</td>
    <td><input type="text" name="a1" class="form-control"></td>
</tr>

<tr><td>Material Price</td>
    <td><input type="text" name="a2" class="form-control"></td>
</tr>

<tr><td>Material Quantity</td>
    <td><input type="text" name="a3" class="form-control"></td>
</tr>
<tr><td>Total Amount</td>
    <td><input type="text" name="a4" class="form-control"></td>
</tr>


<tr><td>Comments</td>

<td><textarea name="a5" rows="4"  class="form-control"></textarea></td>
</tr>

<tr>
    <td>Project Name</td>
<td><select name="a6" class="form-control">
@if (is_array($dropdown1) || is_object($dropdown1))
    @foreach($dropdown1 as $dropdown1values)
    <option value="{{ $dropdown1values->id}}">{{ $dropdown1values->project_name }}</option>
@endforeach
@endif


</select></td></tr>
</tbody>
</table>
<input type="submit" name="a7" class="btn btn-primary">

</form>
</div>
</center>
 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
@endsection