<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\erpproject;
use App\Models\purchase_material;
class sapurchasematerial extends Controller
{
    //
    function senddropdown1(){
       
       
        $user = DB::table('erp_project')
        ->where('user',session('empid'))
        ->get();
        return view('admin.sapurchasematerial',['dropdown1'=>$user]);
        
       

}

function insertdata(Request $req){

            $newid=0;
                    $highestid = DB::table('advance_request')->max('id');
                $newid=$highestid+1;
       
       
    $user = DB::table('advance_request')
    ->insert([
        'id'=>$newid,
        'adv_type'=>'1',
        'category'=>'2',
        'project_id'=>$req->input('a6'),
        'request_amount'=>$req->input('a4'),
        'approval_amount'=>'0',
        'no_days'=>'0',
        //'last_month_salary'=>'0',
        'tenure'=>'0',
        //'approval_status'=>'0',
        //'payment_status'=>'0',
        //'advance_pay_status'=>'0',
        'account_approved_date'=>'1000-01-01 00:00:00',
        //'reimbursement_date'=>date("Y-m-d H:i:s"),
        'account_approved_details'=>'0',
        'comment'=>$req->input('a5'),
        'user_id'=>session('empid'),
        'date'=>date("Y-m-d H:i:s"),
        'from_date'=>'1000-01-01 00:00:00',
        'to_date'=>'1000-01-01 00:00:00',
        'from_location'=>'0',
        'to_location'=>'0',
        'submit_status'=>'0',
        //'confirmation_status'=>'0',
        //'settlement_status'=>'0',
        //'settlement_salary_id'=>'0',
        //'settlement_description'=>'0',
        'last_update'=>'1000-01-01 00:00:00'


    ]);

            $newid2=0;
            $highestid2 = DB::table('material_purchase')->max('id');
        $newid2=$highestid2+1;

    $user2 = DB::table('material_purchase')
    ->insert([
        'id'=>$newid2,
        'adv_request_id'=>$newid,
        'material_name'=>$req->input('a1'),
        'material_price'=>$req->input('a2'),
        'qty'=>$req->input('a3'),
        'total_amount'=>$req->input('a4'),
        'comment'=>$req->input('a5'),
        'user_id'=>session('empid'),
        'date'=>date("Y-m-d H:i:s"),
        


    ]);
  
  
    echo '<script> alert("Report Submitted"); </script>';
    $user = DB::table('erp_project')
            ->where('user',session('empid'))
            ->get();
    return view('admin.sapurchasematerial',['dropdown1'=>$user]);
    
   

}
}
