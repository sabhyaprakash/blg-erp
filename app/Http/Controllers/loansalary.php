<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\erpproject;

class loansalary extends Controller
{
    //
    function insertdata(Request $req){

        $newid=0;
            $highestid = DB::table('advance_request')->max('id');
        $newid=$highestid+1;
        
        
        
        $user = DB::table('advance_request')
        ->insert([
            'id'=>$newid,
            'adv_type'=>'2',
            'category'=>'0',
            'project_id'=>'0',
            'request_amount'=>$req->input('a1'),
            'approval_amount'=>'0',
            'no_days'=>'0',
            //'last_month_salary'=>'0',
            'tenure'=>$req->input('a4'),
            //'approval_status'=>'0',
            //'payment_status'=>'0',
            //'advance_pay_status'=>'0',
            'account_approved_date'=>'1000-01-01 00:00:00',
            //'reimbursement_date'=>date("Y-m-d H:i:s"),
            'account_approved_details'=>'0',
            'comment'=>$req->input('a2'),
            'user_id'=>session('empid'),
            'date'=>date("Y-m-d H:i:s"),
            'from_date'=>'1000-01-01 00:00:00',
            'to_date'=>'1000-01-01 00:00:00',
            'from_location'=>'0',
            'to_location'=>'0',
            'submit_status'=>'0',
            //'confirmation_status'=>'0',
            //'settlement_status'=>'0',
            //'settlement_salary_id'=>'0',
            //'settlement_description'=>'0',
            'last_update'=>'1000-01-01 00:00:00'
        
        
        ]);
        
        echo '<script> alert("Report Submitted"); </script>';
       
        return view('admin.applyloan');
        
        
        
        }
}
