<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class erpproject extends Model
{
    protected $table = 'erp_project';
    use HasFactory;
}
