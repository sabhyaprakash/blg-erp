<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class purchase_material extends Model
{
    protected $table = 'material_purchase';
    use HasFactory;
}
